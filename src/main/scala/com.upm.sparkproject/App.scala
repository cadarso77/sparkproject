package com.upm.sparkproject

import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkConf
import org.apache.spark.ml.{Model, Pipeline, Transformer}
import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.ml.feature.{OneHotEncoder, StringIndexer, VectorAssembler}
import org.apache.spark.ml.regression.LinearRegression
import org.apache.spark.ml.tuning.{CrossValidator, ParamGridBuilder}
import org.apache.spark.ml.util.MLWritable
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.functions._


/**
 * @author ${Manuel Cadarso, Jose Luis Contreras}
 */
object App {

  // User defined functions

  /*
   *  Function to convert times passed as hhmm to minutes
   * param time in format hhmm
   * return time converted to minutes
   */
  val formatTime: UserDefinedFunction = udf {
    (time : Double) =>
    {
      var rest=time%100
      var hour=(time-rest)/100
      hour*60+rest
    }
  }

  /*
   * Classifies a given time as a High, Medium or Low stress moment (based on data exploration,
   * flights departing in between 4pm and 9pm suffer way more delays)
   * param time in minutes (minutes passed since the start of the day)
   */
  val getRushHour: UserDefinedFunction = udf {
    (time : Double) =>
    {
      if (time < 660) // 660 min = 11:00 am
        "Low"
      else if (time >= 1020 && time <= 1260) // 16:00 - 21:00
        "High"
      else
        "Medium"
    }
  }

  /*
   * Checks if the given day belongs to a holiday period
   * (Only checking Christmas in this case, a better although more complex solution
   * should check other public holidays too)
   */
  val isHoliday: UserDefinedFunction = udf {
    (month: Int, day: Int) =>
    {
      // Considering Christmas to range from Dec 20th to Jan 7th
      if ((month == 12 && day > 20) || (month == 1 && day < 7))
        "Christmas"
      else
        "Normal"
    }
  }

  /*
   * Checks if the given month corresponds to a high, medium or low season
   */
  val getSeason: UserDefinedFunction = udf {
    (month: Int) => {
      month match {
        case x if (9 <= x && x <= 11) => "Low"
        case y if (1 <= y && y <= 5) => "Medium"
        case _ => "High"
      }
    }
  }


  def main(args : Array[String]) {

    val t0 = System.currentTimeMillis()

    // Reduce logging verbosity
		Logger.getRootLogger().setLevel(Level.WARN)


		val sparkConf = new SparkConf().setAppName("Big data")
      .setMaster("local")

		val spark = SparkSession
  		.builder()
			.config(sparkConf)
  		.getOrCreate()

		import spark.implicits._

    // Load the data
    var df = spark
  		.read
  		.format("com.databricks.spark.csv")
  		.option("sep", ",")
  		.option("header", "true")
      .load(args(0))
      .select(
  			col("Month"),
        col("DayofMonth"),
        col("Year"),
        col("DayOfWeek"),
        formatTime(col("CRSDepTime")).as("CRSDepTime"),
        formatTime(col("CRSArrTime")).as("CRSArrTime"),
        col("DepDelay").cast("double"),
        col("Origin"),
        col("Dest"),
  			col("Distance").cast("double"),
        col("FlightNum"),
        col("ArrDelay").cast("double"),
        col("TaxiOut").cast("double"))
      .filter($"Cancelled" === 0)    // Filter out all cancelled flights, they are useless for us (1 = cancelled)
      .filter($"ArrDelay".isNotNull) // Rows with ArrDelay = null are useless for us (about 2% of lines in the 2008 DS)
      .filter($"CRSArrTime".isNotNull)
      .filter($"Distance".isNotNull)
      .withColumn("RushHour", getRushHour(col("CRSDepTime")))
      .withColumn("Season", getSeason(col("Month")))
      .withColumn("DayType", isHoliday(col("Month"), col("DayofMonth")))

    var numerical = Array("DepDelay", "Distance", "CRSArrTime")

    // Handling null values of TaxiOut: if more than 30% of the rows don't contain it, don't use it
    if ((df.filter($"TaxiOut".isNotNull).count*100 / df.count > 70)){
      numerical = numerical :+ "TaxiOut"
      df = df.filter($"TaxiOut".isNotNull)
    }

    val categoricalVars = Array("DayOfWeek","RushHour", "Dest", "Origin", "Season", "DayType", "Year")

    def filterArray(colName: String) = (df.select(colName).distinct().count()>1)

    println("Filtering categorical variables...")
    var t0_str = System.currentTimeMillis()

    val usedCategoricalVars = {
      categoricalVars.filter(filterArray)
    }

    println("Categorical variables filtered [" + (System.currentTimeMillis() - t0_str)/1000 + "s]")

    println("Creating StringIndexers")
    t0_str = System.currentTimeMillis()
    val indexTransformers: Array[org.apache.spark.ml.PipelineStage] = usedCategoricalVars.map(
      (cname: String) => new StringIndexer()
        .setInputCol(cname)
        .setOutputCol(s"${cname}_index")
        .fit(df)
    )
    val t1_str = System.currentTimeMillis()
    println("StringIndexers created [" + (t1_str - t0_str)/1000 + "s]")

    // Encode categorical variables with OneHotEncoder so that they can be interpreted correctly
    val oneHotEncoders: Array[org.apache.spark.ml.PipelineStage] = usedCategoricalVars.map(
      (cname: String) => new OneHotEncoder()
        .setInputCol(s"${cname}_index")
        .setOutputCol(s"${cname}_vector")
    )

    // Split data into training and test sets
    val split = df.randomSplit(Array(0.7,0.3), seed = 12345)
    val training = split(0)
    val test = split(1)

    // Vector assembler to prepare the data for the model
    val assembler = new VectorAssembler()
      .setInputCols(numerical ++ usedCategoricalVars.map( x => x + "_vector"))
      .setOutputCol("features")

    val lr = new LinearRegression()
      .setFeaturesCol("features")
      .setLabelCol("ArrDelay")
      .setMaxIter(30)
      .setSolver("l-bfgs")

    val evaluator = new RegressionEvaluator()
      .setLabelCol("ArrDelay")
      .setMetricName("r2")

    // We use a ParamGridBuilder to construct a grid of parameters to search over.
    // this grid will have 3 x 2 = 6 parameter settings for CrossValidator to choose from.
    val paramGrid = new ParamGridBuilder()
        .addGrid(lr.elasticNetParam, Array(0.4, 0.6, 0.8))
        .addGrid(lr.regParam, Array(0.1, 0.01))
        .build()

    val pipeline1 = new Pipeline()
      .setStages(indexTransformers ++ oneHotEncoders ++ Array(assembler, lr))


    // We now treat the Pipeline as an Estimator, wrapping it in a CrossValidator instance.
    // This will allow us to jointly choose parameters for all Pipeline stages.
    // A CrossValidator requires an Estimator, a set of Estimator ParamMaps, and an Evaluator.
    // Note that the evaluator here is a BinaryClassificationEvaluator and its default metric
    // is areaUnderROC.
    val cv = new CrossValidator()
      .setEstimator(pipeline1)
      .setEvaluator(evaluator)
      .setEstimatorParamMaps(paramGrid)
      .setNumFolds(5)  // Use 3+ in practice

    // Run cross-validation, and choose the best set of parameters.
    var cvModel: Transformer = null

    if(args(1) == 0){
      cvModel = cv.fit(training)
    }else if(args(1) == 1){
      cvModel = pipeline1.fit(training)
    }

    val eval_mae = new RegressionEvaluator()
      .setLabelCol("ArrDelay")
      .setMetricName("mae")

    val eval_r2 = new RegressionEvaluator()
      .setLabelCol("ArrDelay")
      .setMetricName("rmse")


    val predictions = cvModel.transform(test)
    val rmse = evaluator.evaluate(predictions)
    val mae = eval_mae.evaluate(predictions)
    val r2 = eval_r2.evaluate(predictions)
    df.select(avg($"ArrDelay")).show()
    println("Mean absolute error: " + mae)
    println("R-squared: " + rmse)
    println("RMSE: " + r2)

    val t1 = System.currentTimeMillis()

    println("Elapsed time: " + ((t1 - t0)/1000) + " s")

  }

}
